<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190415084107 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE info_personelle DROP FOREIGN KEY FK_E02B9582FCC57A9');
        $this->addSql('DROP INDEX UNIQ_E02B9582FCC57A9 ON info_personelle');
        $this->addSql('ALTER TABLE info_personelle DROP inherit_id, DROP parent, DROP enfant');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE info_personelle ADD inherit_id INT DEFAULT NULL, ADD parent INT DEFAULT NULL, ADD enfant INT DEFAULT NULL');
        $this->addSql('ALTER TABLE info_personelle ADD CONSTRAINT FK_E02B9582FCC57A9 FOREIGN KEY (inherit_id) REFERENCES user (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E02B9582FCC57A9 ON info_personelle (inherit_id)');
    }
}
