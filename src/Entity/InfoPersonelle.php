<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\InfoPersonelleRepository")
 */
class InfoPersonelle implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $adresse;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min="5", minMessage="Votre code postal doit comporter 5 chiffre")
     */
    private $codePostal;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ville;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min="8", minMessage="Votre numéro doit comporter 10 chiffre")
     */
    private $tel;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(min="8", minMessage="Votre numéro doit comporter 10 chiffre")
     */
    private $telPort;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(min="14", minMessage="Votre numéro siret doit comporter 14 Chiffre Minimun")
     */
    private $siret;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $raisonSocial;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $naf;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $personneAContacter;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Resultat", mappedBy="Operateur")
     */
    private $resultats;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Resultat", mappedBy="parent")
     */
    private $results;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min="10", minMessage="Votre mot de passe doit contenir minimun 10 caractères")
     * @Assert\EqualTo(propertyPath="confirm_password", message="Vous n'avez pas entrée le même mot de passe")
     */
    private $password;
    
    /**
     * @Assert\EqualTo(propertyPath="password",  message="Vous n'avez pas entrée le même mot de passe")
     */
    public $confirm_password;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $numeroPoleEmploi;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateNaissance;

    
    public function __construct()
    {
        $this->resultats = new ArrayCollection();
        $this->results = new ArrayCollection();
    }

   

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getCodePostal(): ?string
    {
        return $this->codePostal;
    }

    public function setCodePostal(string $codePostal): self
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getTel(): ?string
    {
        return $this->tel;
    }

    public function setTel(string $tel): self
    {
        $this->tel = $tel;

        return $this;
    }

    public function getTelPort(): ?string
    {
        return $this->telPort;
    }

    public function setTelPort(?string $telPort): self
    {
        $this->telPort = $telPort;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getSiret(): ?string
    {
        return $this->siret;
    }

    public function setSiret(?string $siret): self
    {
        $this->siret = $siret;

        return $this;
    }

    public function getRaisonSocial(): ?string
    {
        return $this->raisonSocial;
    }

    public function setRaisonSocial(?string $raisonSocial): self
    {
        $this->raisonSocial = $raisonSocial;

        return $this;
    }

    public function getNaf(): ?string
    {
        return $this->naf;
    }

    public function setNaf(?string $naf): self
    {
        $this->naf = $naf;

        return $this;
    }

    public function getPersonneAContacter(): ?string
    {
        return $this->personneAContacter;
    }

    public function setPersonneAContacter(string $personneAContacter): self
    {
        $this->personneAContacter = $personneAContacter;

        return $this;
    }

    

    


    /**
     * @return Collection|Resultat[]
     */
    public function getResultats(): Collection
    {
        return $this->resultats;
    }

    public function addResultat(Resultat $resultat): self
    {
        if (!$this->resultats->contains($resultat)) {
            $this->resultats[] = $resultat;
            $resultat->addOperateur($this);
        }

        return $this;
    }

    public function removeResultat(Resultat $resultat): self
    {
        if ($this->resultats->contains($resultat)) {
            $this->resultats->removeElement($resultat);
            $resultat->removeOperateur($this);
        }

        return $this;
    }


    /**
     * @return Collection|Resultat[]
     */
    public function getResults(): Collection
    {
        return $this->results;
    }

    public function addResult(Resultat $result): self
    {
        if (!$this->results->contains($result)) {
            $this->results[] = $result;
            $result->setParent($this);
        }

        return $this;
    }

    public function removeResult(Resultat $result): self
    {
        if ($this->results->contains($result)) {
            $this->results->removeElement($result);
            // set the owning side to null (unless already changed)
            if ($result->getParent() === $this) {
                $result->setParent(null);
            }
        }

        return $this;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getNumeroPoleEmploi(): ?string
    {
        return $this->numeroPoleEmploi;
    }

    public function setNumeroPoleEmploi(?string $numeroPoleEmploi): self
    {
        $this->numeroPoleEmploi = $numeroPoleEmploi;

        return $this;
    }

    

    public function eraseCredentials(){}
    
    public function getSalt(){}

    public function getRoles(){
        return ['ROLE_USER'];
    }

    public function getDateNaissance(): ?\DateTimeInterface
    {
        return $this->dateNaissance;
    }

    public function setDateNaissance(\DateTimeInterface $dateNaissance): self
    {
        $this->dateNaissance = $dateNaissance;

        return $this;
    }
   
}
