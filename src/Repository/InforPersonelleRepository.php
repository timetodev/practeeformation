<?php

namespace App\Repository;

use App\Entity\InforPersonelle;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method InforPersonelle|null find($id, $lockMode = null, $lockVersion = null)
 * @method InforPersonelle|null findOneBy(array $criteria, array $orderBy = null)
 * @method InforPersonelle[]    findAll()
 * @method InforPersonelle[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InforPersonelleRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, InforPersonelle::class);
    }

    // /**
    //  * @return InforPersonelle[] Returns an array of InforPersonelle objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?InforPersonelle
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
