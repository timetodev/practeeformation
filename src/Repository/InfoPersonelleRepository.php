<?php

namespace App\Repository;

use App\Entity\InfoPersonelle;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method InfoPersonelle|null find($id, $lockMode = null, $lockVersion = null)
 * @method InfoPersonelle|null findOneBy(array $criteria, array $orderBy = null)
 * @method InfoPersonelle[]    findAll()
 * @method InfoPersonelle[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InfoPersonelleRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, InfoPersonelle::class);
    }

    public function findByDemande($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.numeroPoleEmploi != :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findByEntreprise($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.siret != :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getResult()
        ;
    }

    // /**
    //  * @return InfoPersonelle[] Returns an array of InfoPersonelle objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?InfoPersonelle
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
