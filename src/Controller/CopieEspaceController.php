<?php

namespace App\Controller;

use App\Entity\InfoPersonelle;
use App\Repository\InfoPersonelleRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CopieEspaceController extends AbstractController
{
    /**
     * @Route("/copie", name="copie_espace")
     */
    public function espaceperso(InfoPersonelleRepository $repo, ObjectManager $manager)
    {
        $id = null;
        $info = new InfoPersonelle();
        $info = $repo->findOneBy(['prenom' => 'Hugo']);

        
    
       
        
        

        return $this->render('copie_espace/index.html.twig', [
            'json' => $info,
        ]);
    }


       /**
     * @Route("/copie/{id}", name="copie_profil")
     *
     */
    public function profil(ObjectManager $manager, $id)
    {
     
      
        
        /* Tableau de suivie de formation de la page profil */
        if($id == 1){
            /*  Tableau de Jean Louis #1*/
            $tab = 
            [
                
                [
                    'id' => '1',
                    'name' => 'Enveloppe performantes',
                    'note' => '100',
                    'user' => 'Jean Louis'
                ],
                [
                    'id' => '1',
                    'name' => 'Menuiserie Performantes',
                    'note' => '100',
                    'user' => 'Jean Louis'
                ],
                [
                    'id' => '1',
                    'name' => 'Ventilation Performantes',
                    'note' => '100',
                    'user' => 'Jean Louis'
                ],
                [
                    'id' => '1',
                    'name' => 'PRAXIBAT Parois Opaques',
                    'note' => '100',
                    'user' => 'Jean Louis'
                ],
                [
                    'id' => '1',
                    'name' => 'PRAXIBAT Ventilation',
                    'note' => '100',
                    'user' => 'Jean Louis'
                ],
                [
                    'id' => '1',
                    'name' => 'Formation Intégrée au travail',
                    'note' => '100',
                    'user' => 'Jean Louis'
                ],
                [
                    'id' => '1',
                    'name' => 'Formation En situation de travail',
                    'note' => '100',
                    'user' => 'Jean Louis'
                ],
                [
                    'id' => '1',
                    'name' => 'Formation à l\'utilisation des outils pédagogique',
                    'note' => '100',
                    'user' => 'Jean Louis'
                ],
                



               
            ];
        }
        else if($id == 2){
            $tab = 
            [
                
                [
                    'id' => '1',
                    'name' => 'PRAXIBAT Parois opaques',
                    'note' => '100',
                    'user' => 'Patrice'
                ]
              
            ];
        }
        else if($id == 3){
            $tab = 
            [
                [
                    'id' => '1',
                    'name' => 'Enveloppe performantes',
                    'note' => '100',
                    'user' => 'Hubert'
                ],
                [
                    'id' => '1',
                    'name' => 'Menuiserie Performantes',
                    'note' => '100',
                    'user' => 'Hubert'
                ],
                [
                    'id' => '1',
                    'name' => 'Ventilation Performantes',
                    'note' => '100',
                    'user' => 'Hubert'
                ],
                [
                    'id' => '1',
                    'name' => 'PRAXIBAT Parois Opaques',
                    'note' => '100',
                    'user' => 'Hubert'
                ],
                [
                    'id' => '1',
                    'name' => 'PRAXIBAT Ventilation',
                    'note' => '100',
                    'user' => 'Hubert'
                ],
                [
                    'id' => '1',
                    'name' => 'Formation Intégrée au travail',
                    'note' => '100',
                    'user' => 'Hubert'
                ],
                [
                    'id' => '1',
                    'name' => 'Formation En situation de travail',
                    'note' => '100',
                    'user' => 'Jean Louis'
                ],
                [
                    'id' => '1',
                    'name' => 'Formation à l\'utilisation des outils pédagogique',
                    'note' => '100',
                    'user' => 'Jean Louis'
                ],
               

               
            ];
        }
        else if($id == 4){
            $tab = 
            [
                [
                    'id' => '1',
                    'name' => 'Enveloppe performantes',
                    'note' => '100',
                    'user' => 'Benjamin'
                ],
                [
                    'id' => '1',
                    'name' => 'Menuiserie Performantes',
                    'note' => '100',
                    'user' => 'Benjamin'
                ],
                [
                    'id' => '1',
                    'name' => 'Ventilation Performantes',
                    'note' => '100',
                    'user' => 'Benjamin'
                ],
                [
                    'id' => '1',
                    'name' => 'PRAXIBAT Parois Opaques',
                    'note' => '100',
                    'user' => 'Benjamin'
                ],
                [
                    'id' => '1',
                    'name' => 'PRAXIBAT Ventilation',
                    'note' => '100',
                    'user' => 'Benjamin'
                ],
                [
                    'id' => '1',
                    'name' => 'Formation Intégrée au travail',
                    'note' => '100',
                    'user' => 'Benjamin'
                ],
                [
                    'id' => '1',
                    'name' => 'Formation En situation de travail',
                    'note' => '100',
                    'user' => 'Benjamin'
                ],
                [
                    'id' => '1',
                    'name' => 'Formation à l\'utilisation des outils pédagogique',
                    'note' => '100',
                    'user' => 'Benjamin'
                ],
                [
                    'id' => '1',
                    'name' => 'Formation BIM plement',
                    'note' => '100',
                    'user' => 'Benjamin'
                ],
                [
                    'id' => '1',
                    'name' => 'Formation Formateur',
                    'note' => '100',
                    'user' => 'Benjamin'
                ],
               

               
            ];
        }
        
          
           dump($tab);

        
      
        

        return $this->render('copie_espace/indextwo.html.twig', [
            'tab' => $tab,
        ]);
    }
}
