<?php

namespace App\Repository;

use App\Entity\SlideAnnonce;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method SlideAnnonce|null find($id, $lockMode = null, $lockVersion = null)
 * @method SlideAnnonce|null findOneBy(array $criteria, array $orderBy = null)
 * @method SlideAnnonce[]    findAll()
 * @method SlideAnnonce[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SlideAnnonceRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, SlideAnnonce::class);
    }

    // /**
    //  * @return SlideAnnonce[] Returns an array of SlideAnnonce objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SlideAnnonce
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
