<?php

namespace App\Controller;

use App\Entity\InfoPersonelle;
use App\Form\RegisterAssoType;
use App\Form\RegistersalaType;
use App\Form\RegistrationType;
use App\Form\InfoPersonellType;
use App\Form\RegisterdemandeurType;
use App\Repository\InfoPersonelleRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SecurityController extends AbstractController
{
    /**
     * @Route("/inscription/entreprise", name="security_registration")
     */
    public function registration(Request $request, ObjectManager $manager, UserPasswordEncoderInterface $encoder)
    {

        $user = new InfoPersonelle();
        $form = $this->createForm(RegistrationType::class,$user);
        
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $hash = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($hash);
            $manager->persist($user);
            $manager->flush();
        }

        return $this->render('security/registration_entreprise.html.twig', [
            'form' => $form->createView()
        ]);


       
    }

     /**
     * @Route("/inscription/salarier", name="security_registration_salarier")
     */
    public function registrationSalarier(Request $request, ObjectManager $manager, UserPasswordEncoderInterface $encoder)
    {

        $user = new InfoPersonelle();
        $form = $this->createForm(RegistersalaType::class,$user);
        
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $hash = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($hash);
            $manager->persist($user);
            $manager->flush();
        }

        return $this->render('security/registration_salarier.html.twig', [
            'form' => $form->createView()
        ]);


       
    }

    /**
     * @Route("/inscription/demandeur", name="security_registration_demandeur")
     */
    public function registrationDemandeur(Request $request, ObjectManager $manager, UserPasswordEncoderInterface $encoder)
    {

        $user = new InfoPersonelle();
        $form = $this->createForm(RegisterdemandeurType::class,$user);
        
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $hash = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($hash);
            $manager->persist($user);
            $manager->flush();
        }

        return $this->render('security/registration_demandeur.html.twig', [
            'form' => $form->createView()
        ]);


       
    }

       /**
     * @Route("/inscription/association_administration", name="security_registration_asso")
     */
    public function registrationAsso(Request $request, ObjectManager $manager, UserPasswordEncoderInterface $encoder)
    {

        $user = new InfoPersonelle();
        $form = $this->createForm(RegisterAssoType::class,$user);
        
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $hash = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($hash);
            $manager->persist($user);
            $manager->flush();
        }

        return $this->render('security/registration_asso.html.twig', [
            'form' => $form->createView()
        ]);


       
    }
    
     /**
     * @Route("/inscription", name="inscription_home")
     */
    public function inscriptionType()
    {

        if(isset($_POST['selection'])){
            if($_POST['selection'] == "ent_ind"){
                return $this->redirectToRoute('security_registration');
            }
           else if($_POST['selection'] == "ass_adm"){
                return $this->redirectToRoute('security_registration_asso');
            }
            else if($_POST['selection'] == "sal"){
                return $this->redirectToRoute('security_registration_salarier');
            }
            else if($_POST['selection'] == "demandeur"){
                return $this->redirectToRoute('security_registration_demandeur');
            }
        }

        return $this->render('security/index.html.twig');

    }
    /**
    * @Route("/connexion", name="security_login")
    */ 
    public function login(){
        return $this->render('security/login.html.twig');
    }

    /**
    * @Route("/deconnexion", name="security_logout")
    */ 
    public function logout(){}
}
