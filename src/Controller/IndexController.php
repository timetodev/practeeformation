<?php

namespace App\Controller;

use App\Entity\Categorie;
use App\Entity\Formation;
use App\Entity\Actualiter;
use App\Entity\SlideAnnonce;
use App\Repository\FormationRepository;
use App\Repository\ActualiterRepository;
use App\Repository\SlideAnnonceRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class IndexController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(SlideAnnonceRepository $repo, ActualiterRepository $repoA, ObjectManager $manager)
    {
        $slides = new SlideAnnonce();
       
        $slides = $repo->findAll();

        $actualiter = new Actualiter();
        $limit = 3;
        $offset = null;
        $actualiter = $repoA->findBy(array(), array('id' => "DESC"), 3, $offset);
        return $this->render('index/index.html.twig', [
            'controller_name' => 'IndexController',
            'slides' => $slides,
            'actualiter' => $actualiter
        ]);
    }

        /**
     * @Route("/actualiter/{id}", name="actualiter")
     */
    public function actualiter(SlideAnnonceRepository $repo, ActualiterRepository $repoA, ObjectManager $manager, $id)
    {
        $slides = new SlideAnnonce();
        $slides = $repo->findAll();

        $actualiter = new Actualiter();
        $actualiter = $repoA->find($id);
        return $this->render('index/actualiter.html.twig', [
            'controller_name' => 'IndexController',
            'slides' => $slides,
            'actualiter' => $actualiter
        ]);
    }

      /**
     * @Route("/formation/operateur", name="operateur")
     */
    public function operateur(FormationRepository $repo, ObjectManager $manager)
    {
        $operateur = new Formation();
        $operateur = $repo->FindBy(array('categorie' => '1'));
        return $this->render('formations/operationel.html.twig', [
            'pages' => 'Opérateur',
            'operateurs' => $operateur,
            'controller_name' => 'IndexController',
        ]);
    }

     /**
     * @Route("/formation/encadrement", name="encadrement")
     */
    public function encadrement()
    {
        return $this->render('formations/operationel.html.twig', [
            'pages' => 'Encadrement',
            'controller_name' => 'IndexController',
        ]);
    }

    /**
     * @Route("/formation/maitrise", name="maitrise")
     */
    public function maitrise()
    {
        return $this->render('formations/operationel.html.twig', [
            'pages' => 'Maitrise D\'oeuvre',
            'controller_name' => 'IndexController',
        ]);
    }

    /**
     * @Route("/formation/formateurs", name="formateurs")
     */
    public function formateurs()
    {
        return $this->render('formations/operationel.html.twig', [
            'pages' => 'Formateur',
            'controller_name' => 'IndexController',
        ]);
    }
}
