<?php

namespace App\Form;

use App\Entity\InfoPersonelle;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class RegisterAssoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('prenom')
            ->add('adresse')
            ->add('dateNaissance', BirthdayType::class,['format' => 'yyyy/MM/dd'])
            ->add('codePostal', IntegerType::class)
            ->add('ville')
            ->add('tel', NumberType::class)
            ->add('telPort', NumberType::class)
            ->add('email')
            ->add('raisonSocial')
            ->add('personneAContacter')
            ->add('username')
            ->add('password', PasswordType::class)
            ->add('confirm_password', PasswordType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => InfoPersonelle::class,
        ]);
    }
}
