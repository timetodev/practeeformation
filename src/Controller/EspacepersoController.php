<?php

namespace App\Controller;

use App\Entity\Parcours;
use App\Entity\Resultat;
use App\Entity\Formation;
use App\Entity\SlideAnnonce;
use Spipu\Html2Pdf\Html2Pdf;
use App\Entity\InfoPersonelle;
use App\Repository\ParcoursRepository;
use App\Repository\ResultatRepository;
use App\Repository\SlideAnnonceRepository;
use App\Repository\InfoPersonelleRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class EspacepersoController extends AbstractController
{

        /**
     * @Route("/monespace", name="espace_perso")
     *
     */
    public function espaceperso(InfoPersonelleRepository $repo,ResultatRepository $repotwo, SlideAnnonceRepository $repos, ObjectManager $manager)
    {
        $id = null;
        $user = "Benjamin";
        $info = new InfoPersonelle();
        $info = $repo->findOneBy(['prenom' => 'Benjamin']);
     
        $slides = new SlideAnnonce();
        $slides = $repos->findAll();
     
        $resultat = new Resultat();
        $resultat = $repotwo->findAll();
        
        

        return $this->render('espaceperso/index.html.twig', [
            'json' => $info,
            'resultats' => $resultat,
            'slides' => $slides,
            'controller_name' => 'IndexController',
        ]);
    }


       /**
     * @Route("/monespace/{id}", name="userprofil")
     *
     */
    public function profil(ResultatRepository $repo, SlideAnnonceRepository $repos, ObjectManager $manager, $id)
    {
     
        $slides = new SlideAnnonce();
        $slides = $repos->findAll();
        
        /* Tableau de suivie de formation de la page profil */
        if($id == 1){
            /*  Tableau de Jean Louis #1*/
            $tab = 
            [
                
                [
                    'id' => '1',
                    'name' => 'Enveloppe performantes',
                    'note' => '100',
                    'user' => 'Jean Louis'
                ],
                [
                    'id' => '1',
                    'name' => 'Menuiserie Performantes',
                    'note' => '100',
                    'user' => 'Jean Louis'
                ],
                [
                    'id' => '1',
                    'name' => 'Ventilation Performantes',
                    'note' => '100',
                    'user' => 'Jean Louis'
                ],
                [
                    'id' => '1',
                    'name' => 'PRAXIBAT Parois Opaques',
                    'note' => '100',
                    'user' => 'Jean Louis'
                ],
                [
                    'id' => '1',
                    'name' => 'PRAXIBAT Ventilation',
                    'note' => '100',
                    'user' => 'Jean Louis'
                ],
                [
                    'id' => '1',
                    'name' => 'Formation Intégrée au travail',
                    'note' => '100',
                    'user' => 'Jean Louis'
                ],
                [
                    'id' => '1',
                    'name' => 'Formation En situation de travail',
                    'note' => '100',
                    'user' => 'Jean Louis'
                ],
                [
                    'id' => '1',
                    'name' => 'Formation à l\'utilisation des outils pédagogique',
                    'note' => '100',
                    'user' => 'Jean Louis'
                ],
                



               
            ];
        }
        else if($id == 2){
            $tab = 
            [
                
                [
                    'id' => '1',
                    'name' => 'PRAXIBAT Parois opaques',
                    'note' => '100',
                    'user' => 'Patrice'
                ]
              
            ];
        }
        else if($id == 3){
            $tab = 
            [
                [
                    'id' => '1',
                    'name' => 'Enveloppe performantes',
                    'note' => '100',
                    'user' => 'Hubert'
                ],
                [
                    'id' => '1',
                    'name' => 'Menuiserie Performantes',
                    'note' => '100',
                    'user' => 'Hubert'
                ],
                [
                    'id' => '1',
                    'name' => 'Ventilation Performantes',
                    'note' => '100',
                    'user' => 'Hubert'
                ],
                [
                    'id' => '1',
                    'name' => 'PRAXIBAT Parois Opaques',
                    'note' => '100',
                    'user' => 'Hubert'
                ],
                [
                    'id' => '1',
                    'name' => 'PRAXIBAT Ventilation',
                    'note' => '100',
                    'user' => 'Hubert'
                ],
                [
                    'id' => '1',
                    'name' => 'Formation Intégrée au travail',
                    'note' => '100',
                    'user' => 'Hubert'
                ],
                [
                    'id' => '1',
                    'name' => 'Formation En situation de travail',
                    'note' => '100',
                    'user' => 'Jean Louis'
                ],
                [
                    'id' => '1',
                    'name' => 'Formation à l\'utilisation des outils pédagogique',
                    'note' => '100',
                    'user' => 'Jean Louis'
                ],
               

               
            ];
        }
        else if($id == 4){
            $tab = 
            [
                [
                    'id' => '1',
                    'name' => 'Enveloppe performantes',
                    'note' => '100',
                    'user' => 'Benjamin'
                ],
                [
                    'id' => '1',
                    'name' => 'Menuiserie Performantes',
                    'note' => '100',
                    'user' => 'Benjamin'
                ],
                [
                    'id' => '1',
                    'name' => 'Ventilation Performantes',
                    'note' => '100',
                    'user' => 'Benjamin'
                ],
                [
                    'id' => '1',
                    'name' => 'PRAXIBAT Parois Opaques',
                    'note' => '100',
                    'user' => 'Benjamin'
                ],
                [
                    'id' => '1',
                    'name' => 'PRAXIBAT Ventilation',
                    'note' => '100',
                    'user' => 'Benjamin'
                ],
                [
                    'id' => '1',
                    'name' => 'Formation Intégrée au travail',
                    'note' => '100',
                    'user' => 'Benjamin'
                ],
                [
                    'id' => '1',
                    'name' => 'Formation En situation de travail',
                    'note' => '100',
                    'user' => 'Benjamin'
                ],
                [
                    'id' => '1',
                    'name' => 'Formation à l\'utilisation des outils pédagogique',
                    'note' => '100',
                    'user' => 'Benjamin'
                ],
                [
                    'id' => '1',
                    'name' => 'Formation BIM plement',
                    'note' => '100',
                    'user' => 'Benjamin'
                ],
                [
                    'id' => '1',
                    'name' => 'Formation Formateur',
                    'note' => '100',
                    'user' => 'Benjamin'
                ],
               

               
            ];
        }
        
          
           dump($tab);

        
      
        

        return $this->render('espaceperso/profil.html.twig', [
            'slides' => $slides,
            'tab' => $tab,
            'controller_name' => 'IndexController',
        ]);
    }

    /**
     * @Route("/facture", name="generatefacture")
     */
    public function generatepdf(){
      
        $content = '
        <div class="entete" style=" padding: 10px; width:100%; background-color:#333; height: 45px; color: white;"><p style="margin-left: 80%;">Practée Formation Facture numéro :'.rand(2,15).'</p> 
        <img style="margin-top: -50px; max-width: 250px; max-height: 45px;" src="../Public/images/logo.png">
        </div>

        <table style="margin-top:50px;color: white; text-align:center;">
        
            <tr>
                <th>Description</th>
                <th>Qté</th>
                <th>P.U(HT)</th>
                <th>TVA</th>
                <th>Prix Total HT</th>
            </tr>
        
        
            <tr>
                <td>Formation Initial 1</td>
                <td>1</td>
                <td>1200€</td>
                <td>20</td>
                <td>1003.34€ HT</td>
            </tr>
        
        </table>
        ';
        $html2pdf = new HTML2PDF('A4', 'fr');
        $html2pdf->writeHTML($content);
        $html2pdf->output();


        return $this->render('espaceperso/pdf.html.twig');
    }

    /**
     * @Route("/json", name="json")
     */
    public function encodeData(ParcoursRepository $repo, ObjectManager $manager){
        $parcours = new Parcours();
        
        $parcours = $repo->findAll();
        $json = json_encode($parcours);
        
        // Nom du fichier à créer
            $nom_du_fichier = 'parcours.json';

            // Ouverture du fichier
            $fichier = fopen($nom_du_fichier, 'w+');

            // Ecriture dans le fichier
            fwrite($fichier, $json);

            // Fermeture du fichier
            fclose($fichier);


    }

     /**
     * @Route("/test", name="test")
     */
    public function test(){
      
      
        return $this->render('espaceperso/test.html.twig');
    }
    
}
