<?php
namespace App\Controller;

use App\Entity\Formation;
use App\Entity\InfoPersonelle;
use App\Repository\FormationRepository;
use App\Repository\InfoPersonelleRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AdminController as BaseAdminController;

class AdminController extends BaseAdminController
{
    /**
     * @Route("/dashboard", name="admin_dashboard")
     */
    public function dashboard(InfoPersonelleRepository $userrepo, FormationRepository $formrepo, ObjectManager $manager)
    {
        $user = new InfoPersonelle();
        $user = $userrepo->findAll();

        $formation = new Formation();
        $formation = $formrepo->findAll();
        
        $value ='';
        $jobs = $manager->getRepository(InfoPersonelle::class)->findByDemande($value);

        $jobs_ent = $manager->getRepository(InfoPersonelle::class)->findByEntreprise($value);
        // $emploi = new InfoPersonelle();
      
       
            $count_pole = count($jobs);
            $count_ent = count($jobs_ent);
        
        
        
       


        $countForma = count($formation);
        $count = count($user);
        return $this->render('admin/dashboard.html.twig',[
            'count' => $count,
            'ent' => $count_ent,
            'countPole' => $count_pole,
            'countforma' => $countForma,
        ]);
    }
}
