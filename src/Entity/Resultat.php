<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ResultatRepository")
 */
class Resultat
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $note;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $user;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\InfoPersonelle", inversedBy="resultats")
     */
    private $Operateur;

   

    public function __construct()
    {
        $this->Operateur = new ArrayCollection();
    }

  

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function setNote(?string $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getUser(): ?string
    {
        return $this->user;
    }

    public function setUser(string $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|InfoPersonelle[]
     */
    public function getOperateur(): Collection
    {
        return $this->Operateur;
    }

    public function addOperateur(InfoPersonelle $operateur): self
    {
        if (!$this->Operateur->contains($operateur)) {
            $this->Operateur[] = $operateur;
        }

        return $this;
    }

    public function removeOperateur(InfoPersonelle $operateur): self
    {
        if ($this->Operateur->contains($operateur)) {
            $this->Operateur->removeElement($operateur);
        }

        return $this;
    }

   
  
}
